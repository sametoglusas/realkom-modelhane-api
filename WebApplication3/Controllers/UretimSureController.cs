﻿using System;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using WebApplication3.Models;
using Newtonsoft.Json.Linq;

namespace WebApplication2.Controllers
{
    //[Authorize]

    public class UretimSureController : ApiController
    {
        ModelhaneEntities db = new ModelhaneEntities();
        //public static int id = 0;
        //public static int sure = 0;

        /*Bu fonksiyon Adminin sectigi info suresini getirir */
        // /api/uretimsure/action
        [HttpPost]
        public IHttpActionResult GetTerm(JObject jsonData)
        {
            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    try
                    {
                        string isemri = json.isemri;
                        string urun = json.urun;
                        string zorluk = json.zorluk;

                        int sure = Convert.ToInt32(db.URETIMSURELERI.ToList().Where(i => i.IsEmriTanimi == isemri && i.ZorlukDerecesi == zorluk && i.UrunAdi == urun).FirstOrDefault().PlanlananSure);

                        return Ok(sure);
                    }
                    catch (Exception)
                    {

                        return BadRequest("Yanlıs Gonderi");
                    }

                }
                else
                {
                    return BadRequest("GuvenlikHatasi");
                }
            }
            else
            {
                return BadRequest("JsonHatasi");
            }
        }
    }
}
