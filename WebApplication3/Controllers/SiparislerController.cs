﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using WebApplication3.Models;

namespace WebApplication2.Controllers

{
    public class SiparislerController : ApiController
    {
        ModelhaneEntities db = new ModelhaneEntities();

        /*Bu fonksiyon Admin için tüm siparisleri alır*/
        // /api/values/action 
        [HttpPost]
        public IHttpActionResult GetOrdersByAdmin(JObject jsonData)
        {
            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {

                    //try
                    //{
                        string siparis_durum = json.siparisdurum;
                        string isemri_durum = json.isemridurum;

                        if (isemri_durum.ToString().Equals(""))
                        {

                            var liste = db.SIPARISLER.Select(i => new
                            {
                                siparis_id = i.SiparisID,
                                order_no = i.OrderNo,
                                musteri = i.Musteri,
                                model_no = i.ModelNo,
                                model_adi = i.ModelAdi,
                                siparis_durum = i.Durum,

                            }).Where(i => i.siparis_durum == siparis_durum || i.siparis_durum == "Devam").ToList();

                            return Ok(new { results = liste });
                        }
                        else if (isemri_durum.ToString().Equals("All"))
                        {
                            var liste = db.ISEMRI.Select(i => new
                            {
                                isemri_id = i.IsemriID,
                                order_no = i.SIPARISLER.OrderNo,
                                musteri = i.SIPARISLER.Musteri,
                                model_no = i.SIPARISLER.ModelNo,
                                isemritanimi = i.URETIMSURELERI.IsEmriTanimi,
                                isemrisuresi = i.URETIMSURELERI.PlanlananSure,
                                siparis_durum = i.SIPARISLER.Durum,
                                isemri_durum = i.Durum

                            }).Where(i => i.siparis_durum == siparis_durum && (i.isemri_durum == "Devam" || i.isemri_durum == "Duraklatıldı" || i.isemri_durum == "Atanmis")).ToList();
                                                                                                                               
                            return Ok(new { results = liste });
                        }
                        else if (siparis_durum.Equals(""))
                        {
                            var liste = db.ISEMRI.Select(i => new
                            {
                                isemri_id = i.IsemriID,
                                order_no = i.SIPARISLER.OrderNo,
                                musteri = i.SIPARISLER.Musteri,
                                model_no = i.SIPARISLER.ModelNo,
                                isemritanimi = i.URETIMSURELERI.IsEmriTanimi,
                                isemri_durum = i.Durum

                            }).Where(i => i.isemri_durum == isemri_durum).ToList();

                            return Ok(new { results = liste });
                        }
                        else
                        {

                            var liste = db.CALISMAPLANI.Select(i => new
                            {
                                calisma_id = i.CalismaID,
                                musteri = i.ISEMRI.SIPARISLER.Musteri,
                                order_no = i.ISEMRI.SIPARISLER.OrderNo,
                                model_no = i.ISEMRI.SIPARISLER.ModelNo,
                                model_adi = i.ISEMRI.SIPARISLER.ModelAdi,
                                siparis_durum = i.ISEMRI.SIPARISLER.Durum,
                                isemri_durum = i.ISEMRI.Durum

                            }).Where(i => i.siparis_durum == siparis_durum && i.isemri_durum == isemri_durum).ToList();

                            return Ok(new { results = liste });
                        }
                        

                    //}
                    //catch (Exception)
                    //{
                    //    return Content(HttpStatusCode.OK, "Durumlar Ters");
                    //}
                }
            }

            return Content(HttpStatusCode.OK, "Any object has found!");
        }


        /*Bu fonksiyon Admin için sectigi siparisleri alır*/
        // /api/values/action 
        [HttpPost]
        public IHttpActionResult GetOrderByAdminChangeDesigner(JObject jsonData)
        {
            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int id = Convert.ToInt32(json.isemri_id);
                    int designer_id = Convert.ToInt32(json.designer_id);

                    var list = db.CALISMAPLANI.Where(p => p.ISEMRI.IsemriID == id).Single();

                    list.PersonelID = designer_id;

                    db.SaveChanges();

                    return Content(HttpStatusCode.OK, true);
                }
                else
                {
                    return Content(HttpStatusCode.OK, "token");
                }
            }
            else
            {
                return Content(HttpStatusCode.OK, "json");
            }
        }

        /*Bu fonksiyon Admin için aradıgı siparisleri alır*/
        // /api/values/action 
        //[HttpPost]
        //public IHttpActionResult GetOrderByAdminSearch(JObject jsonData)
        //{
        //    if (jsonData != null)
        //    {

        //        dynamic json = jsonData;
        //        if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
        //        {
        //            string ara = json.ara;

        //            var liste = db.SIPARISLER.Select(i => new
        //            {
        //                siparis_id = i.SiparisID,
        //                order_no = i.OrderNo,
        //                musteri = i.Musteri,
        //                model_no = i.ModelNo,
        //                model_adi = i.ModelAdi,
        //                durum = i.Durum


        //            }).Where(i => i.order_no == ara || i.model_no == ara).ToList();

        //            return Ok(new { results = liste });
        //        }
        //    }

        //    return Content(HttpStatusCode.OK, "Any object has found!");
        //}

        /*Bu fonksiyon Adminin sectigi Siparise gider */
        // /api/values/action 
        [HttpPost]
        public IHttpActionResult GetOneOrderByAdmin(JObject jsonData)
        {
            
            if (jsonData != null) { 
                
                dynamic json = jsonData;
                if (json.id != null && json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int id = json.id;

                    var liste = db.SIPARISLER.Select(i => new
                    {
                        siparis_id = i.SiparisID,
                        order_no = i.OrderNo,
                        musteri = i.Musteri,
                        model_no = i.ModelNo,
                        model_adı = i.ModelAdi

                    }).Where(i => i.siparis_id == id).ToList();

                    return Ok(new { results = liste });
                }
            }

            return Content(HttpStatusCode.BadRequest, "This object has not found!");
          
        }
        
    }
}
