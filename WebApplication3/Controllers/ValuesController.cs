﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication3.Controllers;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{//[Authorize]
    public class ValuesController : ApiController
    {

        ModelhaneEntities db = new ModelhaneEntities();

        string kayıpbas = "";
        string kayıpson = "";

        int saatbas = 0;
        int dakikabas = 0;
        int saatson = 0;
        int dakikason = 0;
        int kayip_id = 0;
        int id = 0;




        // /api/values/action
        /*Bu fonksiyon giris kontrollerini yapar*/
        /*Sonradan Stringden IHttpActionResult*/
        [HttpPost]
        public JObject UserCheck(JObject jsonData)
        {

            JObject back = new JObject();

            if (jsonData != null)
            {
                dynamic json = jsonData;
                dynamic jsonback = back;

                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    try
                    {
                        string tc = json.tc;
                        string pass = json.pass;
                        var worker = db.CALISANLAR.Select(i => new
                        {
                            id = i.PersonelID,
                            tc = i.TCKimlikNo,
                            sifre = i.SİFRE,
                            isadmin = i.IsAdmin
                        }).Where(i => i.tc == tc && i.sifre == pass).ToList();

                        try
                        {
                            bool isadmin = Convert.ToBoolean(worker.FirstOrDefault().isadmin);

                            if (isadmin == true)
                            {

                                jsonback.user_id = worker.FirstOrDefault().id.ToString();
                                jsonback.isAdmin = worker.FirstOrDefault().isadmin;

                                back = jsonback;

                                return back;
                                //return "Admin";
                            }
                            else
                            {
                                jsonback.user_id = worker.FirstOrDefault().id.ToString();
                                jsonback.isAdmin = worker.FirstOrDefault().isadmin;

                                back = jsonback;

                                return back;
                                //return "Calısan";
                            }

                        }
                        catch (Exception)
                        {
                            jsonback.hata = "Hata";

                            back = jsonback;

                            return back;
                            //return "Catch the Exception";
                        }
                    }
                    catch (Exception)
                    {

                        jsonback.hata = "sifre ve tc yok";

                        back = jsonback;

                        return back;
                    }

                }
                else
                {
                    jsonback.hata = "Token";

                    back = jsonback;

                    return back;
                    //return "Security Exception";
                }
            }
            else
            {
                back["hata"] = "Json";

                return back;
                //return "Json Exception";
            }
        }

        // /api/values/action/ 
        /*Bu fonksiyon Admine Designer work_full getirir */
        [HttpPost]
        public IHttpActionResult WorkfullReturn(JObject jsondata)
        {


            if (jsondata != null)
            {
                dynamic json = jsondata;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {

                    string tarih = json.tarih;
                    int id = json.id;

                    var list = db.proc_personel_workfull(tarih, id);

                    int sure = 0;

                    try
                    {
                        sure = Convert.ToInt32(list.Single().Value);
                    }
                    catch (Exception)
                    {

                        sure = 0;
                    }
                    

                    return Ok(sure);

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }
            

        

        // /api/values/action/ 
        /*Bu fonksiyon Admine Designers getirir */
        [HttpPost]
        public IHttpActionResult Alldesigner(JObject jsondata)
        {

            if (jsondata != null)
            {
                dynamic json = jsondata;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {

                    string tarih = json.tarih;

                    var list = db.proc_personel_alldesigner(tarih);

                    return Ok(new { results = list });

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }

            

        }

        // /api/values/action/ 
        /*Bu fonksiyon Designer secilince ona atama yapar */
        [HttpPost]
        public IHttpActionResult DesignerGetOrderByAdmin(JObject jsonData)
        {
            //JObject back = new JObject();
            //ObjectResult liste = null;

            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int siparis_id = Convert.ToInt32(json.siparis_id);
                    int designer_id = Convert.ToInt32(json.designer_id);
                    string isemri = json.isemri;
                    string zorluk = json.zorluk;
                    string urun = json.urun;
                    string date = json.date;
                    string time = json.time;
                    int oncelik = Convert.ToInt32(json.oncelik);

                    int uretim_id = Convert.ToInt32(db.URETIMSURELERI.ToList().Where(i => i.IsEmriTanimi == isemri && i.ZorlukDerecesi == zorluk && i.UrunAdi == urun).FirstOrDefault().UretimID);


                    db.proc_uretim_suresi(uretim_id, siparis_id, designer_id, oncelik,date);
                    

                    return Content(HttpStatusCode.OK, true);
                }
                else
                {
                    return Content(HttpStatusCode.OK, "token");
                }
            }
            else
            {
                return Content(HttpStatusCode.OK, "json");
            }



        }




        // /api/values/action/ 
        /*Bu fonksiyon İsemri Ceker */
        [HttpPost]
        public IHttpActionResult GetWork(JObject jsonData)
        {
            //JObject back = new JObject();
            //ObjectResult liste = null;

            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    var liste = db.ISEMRITANIMI.Select(i => new
                    {
                        id = i.IsEmriID,
                        name = i.IsEmriAdi
                        
                    }).ToList();

                    return Ok(new { results = liste });
                }
                else
                {
                    return Content(HttpStatusCode.OK, "token");
                }
            }
            else
            {
                return Content(HttpStatusCode.OK, "json");
            }



        }


        // /api/values/action/ 
        /*Bu fonksiyon Ürün Ceker */
        [HttpPost]
        public IHttpActionResult GetProduct(JObject jsonData)
        {
            //JObject back = new JObject();
            //ObjectResult liste = null;

            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    var liste = db.URUNLER.Select(i => new
                    {
                        id = i.UrunID,
                        name = i.UrunAdi

                    }).ToList();

                    return Ok(new { results = liste });
                }
                else
                {
                    return Content(HttpStatusCode.OK, "token");
                }
            }
            else
            {
                return Content(HttpStatusCode.OK, "json");
            }



        }

        // /api/values/action/ 
        /*Bu fonksiyon Kayızaman Ceker */
        [HttpPost]
        public IHttpActionResult GetMiss(JObject jsonData)
        {
            //JObject back = new JObject();
            //ObjectResult liste = null;

            if (jsonData != null)
            {

                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    var liste = db.KAYIPZAMANTANIMI.Select(i => new
                    {
                        id = i.TanimID,
                        name = i.KayipZamanTanimi1

                    }).ToList();

                    return Ok(new { results = liste });
                }
                else
                {
                    return Content(HttpStatusCode.OK, "token");
                }
            }
            else
            {
                return Content(HttpStatusCode.OK, "json");
            }



        }

        // /api/values/action/ 
        /*Bu fonksiyon Kayızaman baslatır */
        [HttpPost]
        public IHttpActionResult PostMissPause(JObject jsonData)
        {
            if (jsonData != null)
            {
                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    id = Convert.ToInt32(json.id);

                    var list = db.CALISMAPLANI.Where(p => p.CalismaID  == id).Single();
                                        
                    list.ISEMRI.Durum = "Duraklatıldı";

                    db.SaveChanges();

                    return Ok(true);

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }


        // /api/values/action/ 
        /*Bu fonksiyon Kayızaman bitir  ve sure girer */
        [HttpPost]
        public IHttpActionResult PostMissCont(JObject jsonData)
        {
            if (jsonData != null)
            {
                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    //try
                    //{
                        int id = Convert.ToInt32(json.id);
                        kayip_id = Convert.ToInt32(json.kayip_id);
                        kayıpbas = json.kayip_bas;
                        kayıpson = json.kayip_son;

                    /*burada kayıpzaman bitişleri girip toplam sure hesabı yapıp bunu vt ye girme girisi */

                        int sure = 0;

                        saatbas = Convert.ToInt32(kayıpbas.Substring(0, 2));
                        dakikabas = Convert.ToInt32(kayıpbas.Substring(3, 2));


                        saatson = Convert.ToInt32(kayıpson.ToString().Substring(0, 2));
                        dakikason = Convert.ToInt32(kayıpson.ToString().Substring(3, 2));

                        /*Mesai bitimi*/
                        if (kayip_id == 18)
                        {
                            kayıpbas =  "08:30";
                        }


                        if (saatson - saatbas > 0)
                        {
                            sure = ((saatson - saatbas) * 60 + (dakikason - dakikabas));
                        }
                        else
                        {
                            sure = (dakikason - dakikabas);
                        }

                        KAYIPZAMAN kayıp = new KAYIPZAMAN();
                        kayıp.TanimID = kayip_id;
                        kayıp.CalismaID = id;
                        kayıp.BaslangicSaati = kayıpbas;
                        kayıp.BitisSaati = kayıpson;
                        kayıp.KayıZaman = sure;

                        db.KAYIPZAMAN.Add(kayıp);

                        db.SaveChanges();


                        /*Toplam kayıp zaman calısmaplanına eklemek ıcın*/

                        var list = db.proc_kayıpzaman_sure(id);

                        return Ok(true);
                    //}
                    //catch (Exception)
                    //{

                    //    return Content(HttpStatusCode.BadRequest, "Process Error");
                        
                    //}
                    

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }

        // /api/values/action/ 
        /*Bu fonksiyon Kayızaman baslatır */
        [HttpPost]
        public IHttpActionResult Poststop(JObject jsonData)
        {
            if (jsonData != null)
            {
                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    id = Convert.ToInt32(json.id);


                    var list = db.CALISMAPLANI.Where(p => p.CalismaID == id).Single();
                    
                    list.BitisSaat = DateTime.Now.ToLongTimeString();
                    list.BitisTarih = DateTime.Now.ToString("dd.MM.yyyy");
                    //list.ISEMRI.SIPARISLER.Durum = "Hazır";
                    list.ISEMRI.Durum = "Hazır";

                    db.SaveChanges();

                    return Ok(true);

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }

        // /api/values/action/ 
        /*Bu fonksiyon Kayızaman baslatır */
        [HttpPost]
        public IHttpActionResult Postplay(JObject jsonData)
        {
            if (jsonData != null)
            {
                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    id = Convert.ToInt32(json.id);

                    var list = db.CALISMAPLANI.Where(p => p.CalismaID == id).Single();

                    list.BaslangıcTarih = DateTime.Now.ToString("dd.MM.yyyy");
                    list.BaslangıcSaat = DateTime.Now.ToLongTimeString();
                    list.ISEMRI.SIPARISLER.Durum = "Devam";
                    list.ISEMRI.Durum = "Devam";

                    db.SaveChanges();

                    return Ok(true);

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }


        // /api/values/action/
        /*Bu fonksiyon Adminin sectigi designera atanan siparisleri getirir */
        [HttpPost]
        public IHttpActionResult AllOrderDesignerPostByAdmin(JObject jsonData)
        {
            if (jsonData != null)
            {
                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int id = json.id;
                    var liste = db.CALISMAPLANI.Select(i => new
                    {
                        personel_id = i.PersonelID,
                        siparis_id = i.ISEMRI.SiparisID,
                        model_adi = i.ISEMRI.SIPARISLER.ModelAdi,
                        baslangic_tarih = i.BaslangıcTarih,
                        isinsuresi = i.IsinYapilmaSuresi,
                        isdurum = i.ISEMRI.Durum

                    }).Where(i => i.personel_id == id && i.isdurum != "Hazır").ToList();

                    return Ok(new { results = liste });
                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }
            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }


        // /api/values/action/
        /*Bu fonksiyon Designera tum siparisleri getirir */
        [HttpPost]
        public IHttpActionResult AllOrderPostByDesigner(JObject jsonData)
        {                       
            if (jsonData != null)
            {
                dynamic json = jsonData;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int id = Convert.ToInt32(json.id);

                    var liste = db.CALISMAPLANI.Select(i => new
                    {
                        personel_id = i.PersonelID,
                        calisma_id = i.CalismaID,
                        order_no = i.ISEMRI.SIPARISLER.OrderNo,
                        model_no = i.ISEMRI.SIPARISLER.ModelNo,
                        model_name = i.ISEMRI.SIPARISLER.ModelAdi,
                        kritik = i.ISEMRI.SIPARISLER.KritikBilgi,
                        isintanimi = i.ISEMRI.URETIMSURELERI.IsEmriTanimi,
                        urun = i.ISEMRI.URETIMSURELERI.UrunAdi,
                        zorluk = i.ISEMRI.URETIMSURELERI.ZorlukDerecesi,
                        isinsuresi = i.IsinYapilmaSuresi,
                        isemri_durum = i.ISEMRI.Durum,
                        oncelik = i.Oncelik,
                        plan = i.PlanlamaTarihi

                    }).Where(i => i.personel_id == id && ( i.isemri_durum == "Devam" || i.isemri_durum == "Askida" || i.isemri_durum == "Atanmis" || i.isemri_durum == "Duraklatıldı")).OrderBy(i => i.oncelik).OrderBy(i => i.plan).ToList();

                    return Ok(new { results = liste });

                   
                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }
            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }

        // /api/values/action/
        /*Bu Designer fonksiyon vtye eksik siparis bilgisini girer */
        [HttpPost]
        public IHttpActionResult Postinfoorder (JObject jsondata)
        {
            if (jsondata != null)
            {
                dynamic json = jsondata;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int id = Convert.ToInt32(json.id);
                    string neden = json.neden;

                    var list = db.proc_eksik_bilgi(id,neden);

                    return Ok(true);

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }

        // /api/values/action/
        /*Bu fonksiyon Adminin eksik siparisi getirir */
        [HttpPost]
        public IHttpActionResult Getinfoorder(JObject jsondata)
        {
            if (jsondata != null)
            {
                dynamic json = jsondata;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int id = Convert.ToInt32(json.id);


                    var liste = db.CALISMAPLANI.Select(i => new
                    {
                        
                        calisma_id = i.CalismaID,
                        order_no = i.ISEMRI.SIPARISLER.OrderNo,
                        model_no = i.ISEMRI.SIPARISLER.ModelNo,
                        model_name = i.ISEMRI.SIPARISLER.ModelAdi,
                        isintanimi = i.ISEMRI.URETIMSURELERI.IsEmriTanimi,
                        urun = i.ISEMRI.URETIMSURELERI.UrunAdi,
                        zorluk = i.ISEMRI.URETIMSURELERI.ZorlukDerecesi,
                        aciklama = i.BILGIKONTROL.Aciklama

                    }).Where(i => i.calisma_id == id ).ToList();

                    return Ok(new { results = liste });

                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }

        // /api/values/action/
        /*Bu fonksiyon Adminin eksik siparisine bilgi girer */
        [HttpPost]
        public IHttpActionResult PostInfo(JObject jsondata)
        {
            if (jsondata != null)
            {
                dynamic json = jsondata;
                if (json.token == "a153dd6s33xv6uy9hgf23b16gh")
                {
                    int calisma_id = Convert.ToInt32(json.id);
                    string isemri = json.isemri;
                    string zorluk = json.zorluk;
                    string urun = json.urun;
                    int oncelik = Convert.ToInt32(json.oncelik);

                    int uretim_id = Convert.ToInt32(db.URETIMSURELERI.ToList().Where(i => i.IsEmriTanimi == isemri && i.ZorlukDerecesi == zorluk && i.UrunAdi == urun).FirstOrDefault().UretimID);


                    db.proc_eksik_bilgi_gider(uretim_id, calisma_id, oncelik);

                    return Ok( true);


                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Security Exception");
                }

            }
            else
            {
                return Content(HttpStatusCode.NoContent, "Json Exception");
            }
        }



    }
}
