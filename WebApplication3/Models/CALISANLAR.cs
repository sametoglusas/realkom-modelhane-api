//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CALISANLAR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CALISANLAR()
        {
            this.CALISANPLAN = new HashSet<CALISANPLAN>();
            this.CALISMAPLANI = new HashSet<CALISMAPLANI>();
        }
    
        public int PersonelID { get; set; }
        public string TCKimlikNo { get; set; }
        public string AdiSoyadi { get; set; }
        public string CalistigiBirim { get; set; }
        public string CalistigiBolum { get; set; }
        public string Gorev { get; set; }
        public string IseGirisTarihi { get; set; }
        public Nullable<bool> IsAdmin { get; set; }
        public string SİFRE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CALISANPLAN> CALISANPLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CALISMAPLANI> CALISMAPLANI { get; set; }
    }
}
